# The layout of this Go workspace is a little nonstandard, due to the
# requirement that it live in a repository as a self-contained
# unit. This file attempts to paper over the cracks.
#
# If you run go commands from the shell, they need to be preceded by GOPATH=$PWD

GOPATH=$(shell pwd)

default: build test

build: src/upsided/transitions.go
	GOPATH=$(GOPATH) go build upsided

src/upsided/transitions.go: docs/flow.py
	python docs/flow.py -g >src/upsided/transitions.go

test:
	GOPATH=$(GOPATH) go test sbd

format:
	gofmt -w src/upsided/
	gofmt -w src/blinktest/
	gofmt -w src/lcdtest/
	gofmt -w src/sbd/

lint: src/upsided/transitions.go
	@golint src/upsided 2>&1 | ./lintfilter

clean:
	rm -f upsided blinktest lcdtest
	rm -fr src/upsided/upsided src/upsided/transitions.go
	cd docs; make clean

# Demo programs

lcdtest: src/lcdtest/lcdtest.go
	GOPATH=$(GOPATH) go build lcdtest

blinktest: src/blinktest/blinktest.go
	GOPATH=$(GOPATH) go build blinktest

# One-time production to set up your go environment for this project
setup:
	GOPATH=$(GOPATH) go get -d -u gobot.io/x/gobot/...
	GOPATH=$(GOPATH) go get github.com/BurntSushi/toml

.SUFFIXES: .html .adoc .1

# Requires asciidoc and xsltproc/docbook stylesheets.
.adoc.8:
	a2x --doctype manpage --format manpage $<
.adoc.html:
	a2x --doctype manpage --format xhtml -D . $<
	rm -f docbook-xsl.css
